import express, {Application, Request, Response} from 'express';


const app: Application = express();

app.get('/status', (_, response) => {
    response.status(200).send();
    response.end();
});

app.get('/city', (_, response) => {
    const city = ['Paris', 'Bordeaux', 'Lyon', 'Strasbourg', 'Toulouse', 'Marseille'];
    response.json(city);
    response.end();
});

app.use((_, res) => {
    res.status(404).send('Unable to find the requested resource!');
    res.end();
});



const port = 3000;
app.listen(port, () => {
    console.log(`Server is Fire at http://localhost:${port}`);
});
